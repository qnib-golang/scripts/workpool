/*
Copyright © 2023 Christian Kniep <christian@qnib.org>
*/
package main

import "gitlab.com/qnib-golang/workpool/pkg/cmd"

func main() {
	cmd.Execute()
}

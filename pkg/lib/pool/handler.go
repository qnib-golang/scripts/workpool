package pool

import (
	"math"
	"time"

	"github.com/jedib0t/go-pretty/v6/progress"
	"go.mitsakis.org/workerpool"
)

func Handler(job workerpool.Job[Payload], workerID int, tracker *progress.Tracker) (Result, error) {
	r := Result{}
	r.Sqrt = math.Sqrt(float64(job.Payload.Num))
	time.Sleep(200 * time.Millisecond)
	tracker.Increment(1)
	return r, nil
}

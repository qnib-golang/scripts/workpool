/*
Copyright © 2023 Christian Kniep
*/
package pool

import (
	"fmt"
	"time"

	"github.com/jedib0t/go-pretty/v6/progress"
	"github.com/spf13/cobra"
	"gitlab.com/qnib-golang/workpool/pkg/lib/balken"
	"go.mitsakis.org/workerpool"
)

type Trackers struct {
	List []*progress.Tracker
}

func NewTrackers(pw progress.Writer, cnt int) *Trackers {
	tr := Trackers{
		List: make([]*progress.Tracker, cnt),
	}
	for i := 0; i < cnt; i++ {
		tr.List[i] = &progress.Tracker{
			Message: fmt.Sprintf("wrk%d", i),
			Units:   progress.UnitsDefault,
		}
		pw.AppendTracker(tr.List[i])
	}
	return &tr
}

func deinitWrk(trackers *Trackers) func(workerID int, tr *progress.Tracker) error { // worker deinit function
	return func(workerID int, tr *progress.Tracker) error {
		t := trackers.List[workerID]
		t.MarkAsDone()
		return nil
	}
}

func initWrk(trackers *Trackers) func(workerID int) (*progress.Tracker, error) {
	return func(workerID int) (*progress.Tracker, error) {
		existingTracker := trackers.List[workerID]
		if existingTracker != nil {
			return existingTracker, nil
		} else {
			err := fmt.Errorf("tracker for worker %d does not exist", workerID)
			return nil, err
		}
	}
}

func WorkerPool(cmd *cobra.Command, args []string) {
	r := balken.NewRenderer()
	go r.Render()
	total := 100
	// Tracker for the overall progress of the pool (total of individual workers are not going to be set)
	tracker := progress.Tracker{Message: "Overall", Total: int64(total - 1), Units: progress.UnitsDefault}
	r.AppendTracker(&tracker)
	wrkCnt := 4
	wrkTracker := NewTrackers(r.Writer, wrkCnt)

	p, err := workerpool.NewPoolWithResultsAndInit(4, Handler, initWrk(wrkTracker), deinitWrk(wrkTracker))
	if err != nil {
		panic(err)
	}
	go func() {
		for i := 0; i < total/2; i++ {
			pl := Payload{
				Num: i,
			}
			p.Submit(pl)
		}
	}()
	// Submit half the jobs with delay
	go func() {
		time.Sleep(5 * time.Second)
		for i := 0; i < total/2; i++ {
			pl := Payload{
				Num: i,
			}
			p.Submit(pl)
		}
		p.StopAndWait()
	}()
	for result := range p.Results {
		tracker.Increment(1)
		_ = result
		//tracker.(result.Value.Sqrt)
		//fmt.Println("result:", result.Value)
	}
	time.Sleep(1 * time.Second)
	tracker.MarkAsDone()
	r.Stop()

}

package balken

import (
	"fmt"
	"time"
)

type PinnedMsg struct {
	StartTime time.Time
	Time      string
	Msgs      []string
}

func NewPinnedMsg() *PinnedMsg {
	p := &PinnedMsg{
		StartTime: time.Now(),
		Time:      ">> ",
		Msgs:      []string{},
	}
	p.SetTime(time.Now())
	return p
}

func (p *PinnedMsg) SetTime(t time.Time) {
	msg := fmt.Sprintf(">>   Total Time: %-32s", t.Sub(p.StartTime).Round(time.Millisecond))
	p.Time = msg
}

func (p *PinnedMsg) AddMsg(msg string, args ...interface{}) {
	p.Msgs = append(p.Msgs, fmt.Sprintf(msg, args...))
}

func (p PinnedMsg) String() []string {
	res := []string{
		p.Time,
	}
	return append(res, p.Msgs...)
}

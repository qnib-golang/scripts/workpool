package balken

import (
	"time"

	"github.com/jedib0t/go-pretty/v6/progress"
)

type Renderer struct {
	Writer progress.Writer
	Msg    *PinnedMsg
}

func NewRenderer() (r Renderer) {
	pw := progress.NewWriter()
	pw.SetTrackerLength(50)
	pw.SetUpdateFrequency(time.Millisecond * 100)
	pw.Style().Options.PercentFormat = "%4.1f%%"
	pw.SetSortBy(progress.SortByNone)
	pm := NewPinnedMsg()
	pm.SetTime(time.Now())
	pw.SetPinnedMessages(pm.String()...)
	return Renderer{
		Writer: pw,
		Msg:    pm,
	}
}

func (r *Renderer) Render() {
	r.Writer.Render()
}

func (r *Renderer) AddMsg(msg string, args ...interface{}) {
	r.Msg.AddMsg(msg, args...)
	r.Writer.SetPinnedMessages(r.Msg.String()...)
}

func (r *Renderer) AppendTracker(tracker *progress.Tracker) {
	r.Writer.AppendTracker(tracker)
}

func (r *Renderer) SetTrackerLength(length int) {
	r.Writer.SetTrackerLength(length)
}

func (r *Renderer) Stop() {
	r.Writer.Stop()
}

func (r *Renderer) SetTime(time.Time) {
	r.Msg.SetTime(time.Now())
	r.Writer.SetPinnedMessages(r.Msg.String()...)
}

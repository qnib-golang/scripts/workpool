/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

var version = "0.0.0"
var jsonFlag bool

type Version struct {
	Major int `json:"major"`
	Minor int `json:"minor"`
	Patch int `json:"patch"`
}

// parseCmd represents the parse command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Prints the version of the application",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(PrintVersion(jsonFlag))
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
	// Here you will define your flags and configuration settings.
	versionCmd.Flags().BoolVar(&jsonFlag, "json", false, "Prints the version in JSON format")
	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// parseCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// parseCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func PrintVersion(jFlag bool) string {
	if jFlag {
		vSplit := strings.Split(version, ".")
		major, _ := strconv.Atoi(vSplit[0])
		minor, _ := strconv.Atoi(vSplit[1])
		patch, _ := strconv.Atoi(vSplit[2])
		v := Version{
			Major: major,
			Minor: minor,
			Patch: patch,
		}
		b, err := json.Marshal(v) //, "", "\t")
		if err != nil {
			log.Fatal(err)
		}

		return string(b)
	}
	return fmt.Sprintf("v%s\n", version)
}

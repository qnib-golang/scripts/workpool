# workpool

Example use of [github.com/cmitsakis/workerpool-go](https://github.com/cmitsakis/workerpool-go) with progress bars using [github.com/jedib0t/go-pretty](https://github.com/jedib0t/go-pretty/tree/main/cmd/demo-progress).

![](misc/img/workpool.gif)